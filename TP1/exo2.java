import java.util.*;

public class exo2{
	public static void main(String... args){
		
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		sc.close();

		System.out.println("Valeur de a : " + a);
		System.out.println("Valeur de b : " + b);

		int c = a; //on interchange les valeurs de a et b en utilisant une troisieme variable c
		a = b;
		b = c;
		
		System.out.println("Valeur de a : " + a);
		System.out.println("Valeur de b : " + b);

		System.out.println("Valeur de 2*a : " + 2*a);
		System.out.println("Valeur de b/2 (entier): " + b/2);

		System.out.println("Valeur de a/b : " + a/b);
		System.out.println("Valeur de a%b : " + a%b);

		if(a>=b)
			System.out.println("Valeur la plus grande est a : " + a);
		else
			System.out.println("Valeur la plus grande est b : " + b);
		
	
	}

}
