import java.util.*;

public class exo3{
	public static void main(String... args){
		
		Scanner sc = new Scanner(System.in);
		int annee = sc.nextInt();
		sc.close();

		//l'annee est divisible par 4 mais pas par 100 ou divisible par 400
		if ( (annee%4==0 && annee%100 !=0) || (annee%400==0) )
			System.out.println("L'année est bissextile");
		else
			System.out.println("L'année n'est pas bissextile");
		
		
	
	}

}
