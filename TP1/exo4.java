import java.util.*;

public class exo4{
	public static void main(String... args){
		
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		sc.close();
		
		int secondes = a%60;
		
		a/=60;

		int minutes = a%60;
		
		a/=60;

		int heures = a%24;
		
		a/=24;

		int jours = a;

		System.out.println("secondes : "+secondes);
		System.out.println("minutes : "+minutes);
		System.out.println("heures : "+heures);
		System.out.println("jours : "+jours);

	}

}
