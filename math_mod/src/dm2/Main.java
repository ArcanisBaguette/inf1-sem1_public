package dm2;

public class Main {
	public static void main(String[] args) {
		double[] xi = {4,9,10,14,4,7,12,1,3,8,11,15};
		double[] yi = {39,58,65,73,41,53,60,35,40,59,64,45};
		System.out.println(moyenne(xi));
		System.out.println(moyenne(yi));
		System.out.println(variance(xi));
		System.out.println(covariance(xi,yi));
	}
	
	public static double moyenne(double[] tab) 
	{
		double moyenne=0;
		for(double i : tab) 
			moyenne+=i;
		moyenne/=tab.length;
		return moyenne;
	}
	
	public static double variance(double[] tab) 
	{
		double variance =0;
		double moy = moyenne(tab);
		for(double i : tab)
			variance += (i-moy)*(i-moy);
		variance/=tab.length;
		return variance;
	}
	
	public static double covariance(double[] tabX, double[] tabY) 
	{
		//on consid�re tabX et tabY de m�me taille
		double covariance = 0;
		
		double[] tab_XY = new double[tabX.length];
		for(int i=0; i!=tabX.length; i++)
			tab_XY[i] = tabX[i]*tabY[i];
		
		covariance = moyenne(tab_XY) - moyenne(tabX)*moyenne(tabY);
		
		return covariance;
	}
}
