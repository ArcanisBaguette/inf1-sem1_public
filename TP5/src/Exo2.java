public class Exo2 {
	public static void main(String[] args) 
	{
		int nombres[] = {5,6,7,8,5,6};
		
		System.out.println(premiereOccurence(nombres,6));
		
		System.out.println(nombreOccurence(nombres,6));
		
		remplace(nombres,6,9);
		afficheTab(nombres);		
	}
	
	//2.a
	public static int premiereOccurence(int tab[], int a) 
	{		
		for(int i=0; i!=tab.length; i++)
			if(tab[i]==a)
				return i;
		
		return -1;
	}
	
	//2.b
	public static int nombreOccurence(int tab[], int a) 
	{		
		int nb_occurence=0;
		for(int i=0; i!=tab.length; i++)
			if(tab[i]==a)
				nb_occurence++;
		
		return nb_occurence;
	}
	
	//2.c
	public static void remplace(int tab[], int a, int b) 
	{		
		for(int i=0; i!=tab.length; i++)
			if(tab[i]==a)
				tab[i]=b;
	}
	
	public static void afficheTab(int[] tab) 
	{
		for(int i=0; i!=tab.length; i++)
			System.out.print(tab[i]);
		
		System.out.println();
	}
	
}