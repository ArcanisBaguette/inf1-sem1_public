public class Exo4 {
	public static void main(String[] args) 
	{
		int nombres[] = {5,6,7,8,9};
		
		afficheTab(miroirCopie(nombres));
		
		miroirEnPlace(nombres);
		afficheTab(nombres);
		
	}
	
	//4.a
	public static int[] miroirCopie(int tab[]) 
	{
		int[] copie_miroir= new int[tab.length];
		
		for(int i=0; i!=tab.length; i++)
			copie_miroir[i]=tab[tab.length-i-1];
		
		return copie_miroir;
	}
	
	//4.b
	public static void miroirEnPlace(int tab[]) 
	{		
		for(int i=0; i!=tab.length/2; i++)
			echange(tab,i,tab.length-i-1);		
	}
	
	public static void afficheTab(int[] tab) 
	{
		for(int i=0; i!=tab.length; i++)
			System.out.print(tab[i]);
		
		System.out.println();
	}
	
	public static void echange(int[] tab, int i, int j) 
	{
		int temp = tab[i];
		tab[i]=tab[j];
		tab[j]=temp;
	}
}