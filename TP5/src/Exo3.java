
public class Exo3 {
	public static void main(String[] args) 
	{
		int nombres[] = {0,1};
		int nombress[] = {5,6,7,8};
		
		afficheTab(copie(nombres));
		
		afficheTab(sousTableau(nombress,1,2));
		
		afficheTab(concatenation(nombress,nombres));
		afficheTab(concatenation(nombres,nombress));
		
		afficheTab(fusion(nombress,nombres));
		afficheTab(fusion(nombres,nombress));
		
	}
	
	//3.a
	public static int[] copie(int[] tab) 
	{
		int[] copie = new int[tab.length];
		for(int i=0; i!=tab.length; i++)
			copie[i]=tab[i];
		return copie;
	}
	
	//3.b
	public static int[] sousTableau(int[] tab, int i, int j) 
	{
		int[] copie = new int[j-i];
		for(int k=0; k!=j-i; k++)
			copie[k]=tab[i+k];
		return copie;
	}
	
	//3.c
	public static int[] concatenation(int[] tab1, int[] tab2) 
	{
		int[] concat = new int[tab1.length+tab2.length];
		
		for(int i=0; i!=tab1.length; i++)
			concat[i]=tab1[i];
		
		for(int i=0; i!=tab2.length; i++)
			concat[i+tab1.length]=tab2[i];
		
		return concat;
	}
	
	//3.d
	public static int[] fusion(int[] tab1, int[] tab2) 
	{
		int taille = tab1.length+tab2.length;
		int[] concat = new int[taille];
		
		int premiere_session = 0 ;
		if(tab1.length>tab2.length)
			premiere_session=tab2.length*2;
		else
			premiere_session=tab1.length*2;
		
		for(int i=0; i!=premiere_session; i+=2) 
		{
			concat[i]=tab1[i/2];
			concat[i+1]=tab2[i/2];
		}
		if(tab1.length>tab2.length)
			for(int i=premiere_session, k=premiere_session/2; i!=taille; i++, k++)
				concat[i]=tab1[k];
		
		else
			for(int i=premiere_session, k=premiere_session/2; i!=taille; i++, k++)
				concat[i]=tab2[k];
			
		return concat;
	}
	
	public static void afficheTab(int[] tab) 
	{
		for(int i=0; i!=tab.length; i++)
			System.out.print(tab[i]);
		
		System.out.println();
	}
}