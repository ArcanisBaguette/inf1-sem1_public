import java.util.Scanner;

public class Exo3 {
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		int n = -1 ;
		int somme = 0;
		do 
		{
			n = sc.nextInt();
		}while(n<1);
		
		for(int i=1; i!=n+1; i++) 
		{
			somme+=i;
			System.out.print(i + " + ");
		}
		
		System.out.println("= "+ somme);
		sc.close();
	}
}

/*
 * 
 */