import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Exo4 {
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		int nombre_mystere = ThreadLocalRandom.current().nextInt(1, 100);
		
		int proposition = 0;
		
		do 
		{
			System.out.println("Entrez un nombre entre 1 et 100");
			proposition = sc.nextInt();
			
			if(proposition<nombre_mystere)
				System.out.println("Trop petit !");
			else if(proposition>nombre_mystere)
				System.out.println("Trop grand !");
		}while(proposition!=nombre_mystere);
		
		System.out.println("Gagne");
		sc.close();
	}
}

