import java.util.Scanner;

public class Exo5 {
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);

		int h = -1 ;
		do 
		{
			h = sc.nextInt();
		}while(h<1);
		
		sc.close();
		
		System.out.print("\n");
		//figure 1
		for(int i=0; i!=h; i++) 
		{
			for(int j=0; j!=h-i; j++) 
			{
				System.out.print("*");
			}
			System.out.print("\n");
		}
		
		System.out.print("\n");
		//figure 2
		for(int i=0; i!=h; i++) 
		{
			for(int j=0; j!=h; j++) 
			{
				if( i==0 || j==0 || i==h-1 || j==h-1 )
					System.out.print("*");
				else
					System.out.print(" ");
			}
			System.out.print("\n");
		}
		
		System.out.print("\n");
		//figure 3
		for(int i=0; i!=h; i++) 
		{
			for(int j=0; j!=h; j++) 
			{
				if( i==0 || i==h-1 || j==h-i-1 )
					System.out.print("*");
				else
					System.out.print(" ");
			}
			System.out.print("\n");
		}
		
		System.out.print("\n");
		//figure 4
		for(int i=0; i!=h; i++) 
		{
			for(int espace=0; espace!=h-1-i; espace++) 
				System.out.print(" ");
			
			for(int etoile=0; etoile!=i*2+1; etoile++)
				System.out.print("*");
			
			System.out.print("\n");
		}
		
		System.out.print("\n");
		//figure 5
		for(int i=0; i!=h; i++) 
		{
			for(int j=0; j!=h; j++) 
			{
				if( j==0 || j==h-1 || j==i )
					System.out.print("*");
				else
					System.out.print(" ");
			}
			System.out.print("\n");
		}
		
	}
}

/*
 * 
 */