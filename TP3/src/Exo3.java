
public class Exo3 {
	public static final int TAILLE = 3;
	
	public static void main(String[] args) {
		afficheCible();
	}
	
	//1
	public static void ligneI(int i) 
	{
		int incr=0;
		int index=1;
		
		for(; index!=TAILLE; index++) 
		{
			System.out.print((char)(97+incr));
			if(incr<i)
				incr++;
		}
		
		System.out.print((char)(97+incr));
		index++;
		
		if(incr==TAILLE-1)
			incr--;
		
		for(; index!=TAILLE*2; index++) 
		{
			System.out.print((char)(97+incr));
			if( incr!=i  || TAILLE*2-1-index == i )
				incr--;
		}
		System.out.println();
	}
	
	//2
	public static void afficheCible() 
	{
		
		for (int i = 0; i < TAILLE; i++) {
			ligneI(i);
		}
		for (int i = TAILLE-2; i >= 0; i--) {
			ligneI(i);
		}
	}
}
