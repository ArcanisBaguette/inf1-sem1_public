
public class Exo1 {
	public static void main(String[] args) {
		alphabet_minuscule();
		alphabet_majuscule_envers();
	}
	
	private static void alphabet_minuscule() {
		for(char c='a'; c<='z'; c++)
			System.out.print(c);
		System.out.println();
	}
	
	private static void alphabet_majuscule_envers()	{
		for(char c='Z'; c>='A'; c--)
			System.out.print(c);
		System.out.println();
	}
}
