
public class Exo2 {
	
	private static final double TRANCHE1 = 9807.;
	private static final double TRANCHE2 = 27086.;
	private static final double TRANCHE3 = 72617.;
	private static final double TRANCHE4 = 153783.;
	
	public static void main(String[] args) {
		System.out.println(impot(32000));
		System.out.println(pourcentageImpot(32000));
		
		System.out.println();
		
		System.out.println(impot(10000)+impot(15000));
		System.out.println(impot(12500)*2);
		System.out.println(gainImpotConjoints(10000,15000));
		
		System.out.println();
		
		System.out.println(impot(10000)+impot(27100));
		System.out.println(impot(18550)*2);
		System.out.println(gainImpotConjoints(10000,27100));
	}
	
	public static double min(double a, double b) 
	{
		return a<b ? a : b;
	}
	
	//1
	public static double impot(double revenu) 
	{
		double impot = 0;
		
		if(revenu>TRANCHE1) 
		{
			impot += 0.14 * (min(TRANCHE2,revenu) - TRANCHE1);
			if(revenu>TRANCHE2) 
			{
				impot += 0.3 * (min(TRANCHE3,revenu) - TRANCHE2);
				if(revenu>TRANCHE3) 
				{
					impot += 0.41 * (min(TRANCHE4,revenu) - TRANCHE3);
					if(revenu>TRANCHE4) 
					{
						impot += 0.45 * revenu;
					}
				}
			}
		}
		
		return impot;
	}

	//2
	public static double pourcentageImpot(double revenu) 
	{
		return impot(revenu)/revenu*100;
	}

	public static double gainImpotConjoints(double revenu1, double revenu2) 
	{
		return  impot(revenu1) + impot(revenu2) -  impot( (revenu1+revenu2)/2 )*2;
	}
}
