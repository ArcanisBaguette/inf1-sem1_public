import java.util.Scanner;

public class Exercices {
	public static void main(String[] args) {
		//sommeNnombres(10);
		//tableN(5);
		//tableEntreAetB(7, 5);
		//triangleRectangle(4);
		//triangleFloyd(4);
		//suiteU(3,0);
		suiteFibonacci(15);
		
	}
	
	//1, 2  ??
	public static void sommeNnombres(int n) 
	{
		Scanner sc = new Scanner(System.in);
		
		int somme=0;
		for(int i=0; i!=n; i++) 
		{
			System.out.println("Entrez un nombre");
			somme += sc.nextInt();
		}
		
		System.out.println("La somme est de : " + somme + " et la moyenne de : " + somme/n);
		
		sc.close();
	}

	//3
	public static void tableN(int n) 
	{
		System.out.println("Table de multiplication de "+n+" : ");
		for(int i=1; i<=10; i++)
			System.out.println(i + " x " + n +" = "+n*i);
	}
	
	//4
	public static void tableEntreAetB(int a, int b) 
	{
		int debut = a<b ? a : b;
		int fin = a>b ? a : b;
		
		for(int i=debut; i<=fin; i++)
			tableN(i);
	}
	
	//5
	public static void triangleRectangle(int h) 
	{
		int compteur=0;
		for(int ligne=0; ligne<h; ligne++) 
		{
			for(int k=0; k!=ligne+1; k++)			
				System.out.print(compteur++ + " ");
			System.out.println();
		}
	}
	
	//6
	public static void triangleFloyd(int h) 
	{
		for(int ligne=0; ligne<h; ligne++) 
		{
			for(int k=0; k!=ligne+1; k++)			
				System.out.print((ligne+k+1)%2 + " ");
			System.out.println();
		}
	}
	
	//7
	public static void suiteU(int n, int u) 
	{
		int u_n = u;
		for(int k=1; k<=n; k++) 
		{
			u_n = u_n*2+1;
		}
		System.out.println("u_" + n + " = " + u_n);
	}
	
	//8	
	public static void suiteFibonacci(int n) 
	{
		int result=1;
		if(! ( n==0 || n==1) )
		{
			int f_n=1;
			int f_m=1;
			for (int i = 2; i <= n; i++) {
				result = f_n+f_m;
				
				if(i%2==0) 
					f_n=result;
				
				else 				
					f_m=result;
				
			}
		}
		
		System.out.println(result);
	}
	
}
